---
title: "Bio"
date: 2019-03-24T09:41:39-08:00
description: A little background on how I got here.
hero_image: "hero.jpg"
categories:
- about
nometadata: true
notags: true
noshare: true
nocomments: true
---

## Personal

I'm a mountain boy, from the Rocky Mountain West. I was born in Ft.
Collins, CO and my folks and I moved to Florence, MT in the Bitterroot
Valley when I was six.

I'm a big fan of the Cascadia movement, and I think the Mountain West
needs a similar bioregional separatist movement (the Bitterroot will be
our Kashmir).

## Professional

Right now I'm a Site Reliability Engineer at a brand marketing
consultancy called [Agency Mania Solutions](https://agencymania.com).
That's a fancy way of saying that I'm the first infrastructure hire
there--the founders have followed something like [Erik Dietrick's "Job
Titles: Be Like The
Wolf"](https://daedtech.com/job-titles-be-like-the-wolf/) when it comes
to titles. I wasn't just brought on board to be a sysadmin though, I'm
also doing a lot of application architecture work and driving adoption
of the DevOps methodology across the engineering team.

Prior to that, I've worked as a DevOps engineer embedded with a dev
team, a full stack line of business developer, and a sysadmin at various
companies in Seattle and Missoula.

A solid chunk of the writing you'll find here will be my riffings on
distributed computing, herding nerds, quirks in specific tech that
either aren't well-documented or are worth writing about, and anything
else I find interesting that's even tangentially related to applied
computing. I'm always on the hunt for mentor/mentee relationships as
well, and if you want some guidance or feel you have some I could
benefit from, please don't hesitate to [hit me
up](mailto:contact@ndarwincorn.me).

## Artistic

My medium of choice is sound, and my preferred instruments (in order)
are the bass guitar, cello, banjo and melodica.

Despite identifying as a metalhead above all else, my last two musical
projects that published recordings have been rap. Most recently I tried
(I think we missed the mark) to do an
[over-id](https://www.thefreelibrary.com/Paul+Bowman+and+Richard+Stamp%2c+eds.+The+Truth+of+Zizek.-a0198413970)
project called Brodown. The highlights of that tape are definitely the
features, with first-verses from C-Cow and Doctress (check out Runnin'
wit' my Money) and contributions from Beige Note's Jonnyblazed and
Wormwood.

Right around when we were getting Beige Note off the ground, my death
metal band Akramont was playing the only shows we've played to-date, and
a couple of songs were captured by a fan and have been sitting on
YouTube ever since. To-date they're the only recordings that I have of
me playing metal.

I've got a couple of irons in the fire right now, though it's been slow
going so far. Still, if you like any of the above, keep your eyes peeled
for a fresh rap project and some solo stuff (both rap and instrumental)
coming from me in the near future.

## Athletic

I play flag football with the Seattle Boom, who compete in the
[NGFFL](http://ngffl.com/). Locally I play in the
[CFFL](https://www.cascadeflagfootball.org/), and if you've ever wanted
to play flag or you're looking for a league in Seattle, I can't
recommend it enough. It's plenty competitive, but first-and-foremost we
have fun. I've only been playing for three years now, but you'll probably
find me writing about flag strategy and my expereinces playing soon
enough. I think it's the only way American Football doesn't go the way
of boxing in the long-run.

I'm also a big fan of golf sports, whether traditional or disc. Looking to
pick up foot golf sometime soon too. One of my favorite auxiliary travel
activities is to sneak away from whatever the primary purpose for the
trip is and play a round at a local course, and I've got a backlog of
course reviews that'll go up here some time soon. As mediocre I am at
*playing* any golf sport, I'm a halfway decent caddy. I looped at the
Stock Farm Club in Hamilton, MT for a decade and have been contemplating
picking up some weekend rounds at Chambers Bay, which I may or may not
write about. Solo golf is plenty of fun, but it doesn't hold a candle to
dissecting a course with a caddy, and I enjoy that all the more when I'm
on a gamer's bag.

Living in the North Country is real hard without winter hobbies, and
it's difficult to call yourself a mountain boy if you aren't comfortable
getting out and enjoying the hills. I'm a lot less adventurous than [some
folks I know](http://andrewmayer.blogspot.com/), but I still know how to
get outdoors in all seasons, and there's a good chance I write about
that here too.
