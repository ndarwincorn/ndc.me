---
title: "Cornpetitive Advantage"
date: 2019-03-25T16:53:39-05:00
description: A little background on how we got here.
categories:
- about
nometadata: true
notags: true
noshare: true
nocomments: true
---

## Motivation

This blog's been on my mind for a little bit now. Mostly because I feel
like I haven't been writing enough, but also that I've had a hot-cold
relationship with social media for the last five or six years. Most of
that is their consolodation of the web into digital shopping malls and
their rent-seeking on mindshare.

Some other part of it's the way they use the data I willingly provide them. Not
that I'm particularly paranoid; despite my inflated impression of
myself and the [guvmint's](https://www.muckrock.com/news/archives/2015/sep/24/walt-disneys-fbi-file/)
well-[documented history](https://www.npr.org/2012/02/14/146862081/the-history-of-the-fbis-secret-enemies-list)
of fucking [with people](http://www.waxpoetics.com/blog/features/articles/insidious-tale-actress-jean-seberg-fbis-cointelpro-film-kill/),
I've got just enough self-awareness to recognize that I'm probably not
on any LEO/deep state radar. [That sort of paranoia is mostly the domain
of the militia movement.](https://web.archive.org/web/20180901081358/https://www.intellihub.com/fbi-and-nsa-outsource-their-harassment-of-targeted-individuals-and-watchlisted-individuals-to-private-firms/)

I figure a blog kills two birds with one stone--I can self-publish all
the content that was (in its own small way) making folks like
Zuckerberg more rich and powerful than they already are. Plus my folks
will probably appreciate my radical views being confined to the writing
you'll find here.

## What is Cornpetitive Advantage?

If you take a look at my [bio](/about/bio/#professional-academic), what
probably stands out (now anyways, at the tail end of 2018) is exactly
none of it. Hell, this whole site probably comes across as aggressively
mediocre. I haven't worked anywhere sexy, written any viral blog posts,
contributed as a core member on any big-name open source projects. I
don't even hold a bachelor's degree. My music exists on the internet to
little acclaim. I'm a pretty shitty athlete too. I don't really know any
famous folks either so I can't even starfuck my way to relevance.

Despite that, I've happened to carve out a niche for myself in my early
adulthood. I'd like to think it's because I lucked/privileged my way
into a [competitive
advantage](https://en.wikipedia.org/wiki/Competitive_advantage)
relative to other folks like me. This blog will work to cronicle in a
loose way how I've cultivated that competitive advantage.

This site will present as multidimensional view of me and what makes me
tick as possible. Any digital marketers that come across this'll
probably cringe, but I'm not really trying to build a brand here. More
just trying to document my thoughts and experiences a little bit more
than I have in the past.

## Disclaimer

So far there's only one, I'll add more as necessary:

1. This site is not sanitized.  
   It includes a lot of writing on a lot of different topics. You may
   get here because I've written about a bit of tech, or you may get
   here because I've written a bit about Denver sports, or about
   politics (they're radical leftist, sorrynotsorry). A lot of it may
   offend you, and I'm okay with that. There are two reasons I'd like to
   hear from you if that's the case:

  * If you like only certain things I write about, [let me
    know](mailto:contact@ndarwincorn.me). It won't change the content of
    this site, but I've got some irons in the fire for collaborative
    writing endeavors and that feedback will influence which of those I
    focus on the most.

  * If I write about something and it offends you, and you think I
    need to take another look at my line of thinking, [let me
    know](mailto:contact@ndarwincorn.me). No guarantees that I'll change
    my mind, but I love reasons to reevaluate my worldview.

