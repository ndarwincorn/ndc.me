---
title: "Vim Plugins With Nix"
date: 2020-07-18T08:48:23-07:00
draft: true
description: Declare all the things!
categories:
  - writing
tags:
  - short-form
  - litany
  - process
---

## Introduction

## Pre-reqs

## Caveats

[Neomake](https://github.com/neomake/neomake/issues/2482)

## Process

### Overrides

#### Dependencies

#### Additional Installation

### Git

Including overrides:

```
git add pkg/misc/vim-plugins/overrides.nix
git commit --amend
```

Cleaning empty update commits out of your history:

```
git rebase <branch>~commit#toremove <branch>~1 (most recent commit) <branch>
```
