---
title:
  "An open letter to OSI and Debian signatories to the open letter to the FSF"
hero_image: "hero.jpg"
date: 2021-03-24T07:36:40-07:00
description:
  Stallman should go, but so should the OSI. (CW - Misogyny, Transphobia,
  Ableism, Racism, Colonialism)
categories:
  - writing
tags:
  - short-form
  - rant
draft: false
---

To the signatories of yesterday's
[open letter to the FSF](https://rms-open-letter.github.io/):

Anyone from the OSI ~~signing~~authoring an open letter calling for RMS and the
entire FSF board to step down needs to self-crit and quit the OSI themselves.
Their weaponization of the _real_ problems the software community has with
misogyny, transphobia and ableism in the service of a longstanding grudge is as
offensive as Stallman being back on the FSF board.

## The Stallman problem

Like many, I got really fucking heated yesterday when I saw that RMS announced
his return to the FSF board at LibrePlanet. When I saw the open letter, I was
excited to add my name to the list. Then I noticed that seven of the sixteen
authors are affiliated with Debian or the OSI. We'll get to them, but first we
need to review the target of the letter.

The behavior RMS is getting cancelled[^1] for mostly revolves around his
arch-pedantic nature (as you'll see below, that's not to minimize his very real
harm to the community), including the Epstein/Minsky-related rape apology that
[got the ball rolling eighteen months ago](https://selamjie.medium.com/remove-richard-stallman-fec6ec210794):

- transphobia - RMS refuses to acknowledge
  [the singular they](https://stallman.org/articles/genderless-pronouns.html).
  It's a bad take, but I challenge you to read that post and find a single
  reactionary argument (i.e. bathrooms, etc. etc.) about gender in it.
- ableism - he is
  [ableist as shit towards people with Trisomy 21](https://stallman.org/archives/2016-sep-dec.html#31_October_2016_%28Down%27s_syndrome%29)
- racism (omitted from the letter, but for completeness) - he doesn't like the
  phrase
  ['first nations' and he considers himself a 'Native American' by his definition](https://stallman.org/antiglossary.html).
  He's got some internalized colonialism he needs to interrogate, for sure. But
  that whole page is very clear that he's one of the all-too-typical problematic
  'allies'[^2].

If those were his only transgressions, I would be perfectly ok with a return to
the FSF after demonstrated self-criticism and an actual change in behavior (i.e.
using his clout to decolonize the FSF and ensure that it stays that way, in
addition to never putting bullshit like this into the world again), none of
which I believe he's done. From an intersectional perspective, the body of his
political writing is problematic in places, but redeemable in general. Emphasis
on redeemable--the problematic bits render him pretty useless as an ally right
now.

The behavior he _should_ be cancelled for is demonstrated deep misogyny that
rivals his free software movement in being his most enduring contribution to the
field:

- [Decades of harassment](https://selamjie.medium.com/remove-richard-stallman-appendix-a-a7e41e784f88)
  of women colleagues and undergrads at MIT.
- [His inability to acknowledge](https://geekfeminism.wikia.org/wiki/Richard_Stallman)
  those same colleagues' contributions to his projects:

> - In a 2007 interview, he said:
>
> > I don’t have any experience working with women in programming projects; I
> > don’t think that any volunteered to work on Emacs or GCC.
>
> A number of women have contributed to GCC, including:
>
> - Janis Johnson maintains the test suite and has been a contributor since 2001
> - Sandra Loosemore is the lead author of the GNU Library Reference Manual;[3]
>   RMS is listed as her co-author.
> - Dorit Nuzman made major contributions to loop scheduling and vectorization.
> - Carolyn Tice is also mentioned as a contributor.

## Crocodile tears

Eric Raymond, the founder of the Open Source Initiative and
[randian acolyte](https://firstmonday.org/ojs/index.php/fm/article/view/702/612),
doesn't actually give a shit about ableism, misogyny or transphobia (much like
his objectivist hero). Hilariously on-brand for the outcome of randian
cryptofascism,
[he was given the boot by the monpolists that now run the OSI almost exactly a year ago](https://en.wikipedia.org/wiki/Open_Source_Initiative#2020_Leadership:_Perens_and_ESR).

Being quite public and a prominent 90s tech bro, you can probably guess that
he's written and said
[some horrible shit](http://wehuntedthemammoth.com/2015/11/05/any-woman-in-tech-could-be-a-false-accusing-feminist-honey-trap-addled-open-source-guru-warns/).

Well that's just one co-founder of the OSI, you say. Surely luminaries like
Bruce Perens being involved helped keep that misogyny in check. Oh wait, there's
[Bruce mansplaining](https://geekfeminism.wikia.org/wiki/Category_talk:Candidates_for_deletion/Bruce_Perens)
to the Geek Feminism wiki maintainers that he's reformed and they need to stop
their witch hunt.
[Fuck.](https://geekfeminismdotorg.wordpress.com/2009/08/27/women-in-floss-tell-bruce-perens-you-exist/)
He even does the same shitty [minimization](https://lwn.net/Articles/348541/) of
the women in the field that I'm so mad at RMS for:

> Because women who use EMACS are especially rare. I have found out about only
> one in the past 30 years, and she was trained on it for a job and doesn't use
> it any longer.

To be fair, Eric and Bruce aren't signatories, because they've withdrawn from
public participation in this part of the tech community over their own perceived
cancellation (good). But if you look at the co-authors of the open letter, it's
a who's-who of Debian and OSI folks. In that spirit, let's call this an open
letter for them to withdraw their signatures and authorship until they have
demonstrably rooted out the rampant misogyny from their own projects. They might
claim that the removal of Bruce and Eric from those projects is sufficient, but
as anyone who's ever spent any time in an IRC room or on a mailing list for a
major Linux distribution can tell you, it goes a lot deeper than Eric, Bruce,
and RMS.

Until they do that, the open letter to the FSF seems an awful lot like another
instance of the cooption of critical language (i.e. woke culture) to serve a
[totally unrelated grudge](http://www.softpanorama.org/OSS/ESR_Interviews.shtml#Personal_Attacks_on_RMS).
And time will tell but I'm suspicious that this cooption will be even more
harmful to liberation than the various flavors of misogyny (incel nerds and frat
bros, loosely) that have been endemic to tech for four decades now.

## It's all shit

Frankly, anyone still scratching their head about why women and other
marginalized groups aren't represented in tech isn't paying attention. But if
you want a root cause, it's bigger than tech. The second society no longer
viewed programming as women's work, this current culture of entrenched misogyny
in tech (and software engineering in particular) was inevitable.

ALL of us, especially those in the anglosphere, have to process our deeply
internalized misogyny and colonial beliefs. Absent that, it's going to be
turtles all the way down.

By signing this, I commit to
[combating](https://www.marxists.org/reference/archive/mao/selected-works/volume-2/mswv2_03.htm)
my internalized oppression both introspectively and when it manifests in my
community. I call on the signatories to the
[open letter to the FSF](https://rms-open-letter.github.io/) to rescind their
signatures until they have signed this letter and made that same commitment.

If you would like to add your name, [email me](mailto:contact@ndarwincorn.me).

Signed,

Darwin Corn

[^1]:
    Can you really call it cancellation if he just did the tried-and-true 'step
    down til the scandal blows over' move?

[^2]:
    Among other terms he doesn't like, he includes 'honor killing' because it
    obfuscates the patriarchy behind it.
