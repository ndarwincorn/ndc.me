---
title: "Write Only Social Media"
hero_image: "hero.jpg"
date: 2018-09-03T06:06:06-06:00
description: A call to abandon our digital shopping malls, just as we've abandoned the physical ones.
categories:
- writing
tags:
- long-form
- pontification
- foss
- decentralization
- web3
draft: true
---

## Context

### Shopping Malls

### The Old Web

### Digital Shopping Malls

## Write Only Social Media

### Obstacles

#### Web Scale

#### 
