---
title: "Foreign Interference"
date: 2021-01-25T08:11:51-08:00
draft: false
hero_image: "foreigninterferencehero.png"
description:
  "The real foreign interference that characterizes all public online discourse.
  Hint: it didn't start in 2015."
categories:
  - writing
tags:
  - long-form
  - effortpost
  - rant
  - politics
  - pop-tech
---

tl;dr - Skip to [the end](#combat-astroturfism) if you already know the history
of astroturfing on the internet. If you can point me to instances of
astroturfing or sockpuppet usage from Usenet or earlier internet epochs, please
reach out!

## Russian Troll Farms, A Social Dilemma

Alleged Russian election meddling and disinformation campaigns served as a cash
content crop for US news media for years following the 2016 US Presidential
election. Hell, the original 24-hour news network produced a bulk of that in
order to
[keep the attention it enjoys during the election cycle rolling during the dog days following that cycle](https://archive.is/uV0st#selection-967.119-967.131).
To be clear, I'm not dismissing those allegations. Assuming that the Russian
government doesn't utilize astroturfing out of some misguided critical support
for anyone opposed to Angloimperialism is dangerously naive. But while that's
naive, the credulous sensationalism of the US news media as it reports on
astroturfing performed by Russian (and Chinese) state actors makes such
misguided uncritical support look downright worldly[^1], as it relies on an
ahistorical understanding of how disinformation campaigns are and have been
waged online in the service of states, organizations and individuals. This post
should serve as a primer for at least a little slice of that history.

Following on Russiagate's coattails, "A Social Dilemma" has found a receptive
audience among a public already 'educated' on the way state and non-state actors
foment misinformation online, chief among them the Russian Internet Research
Agency[^2].

[But there are many reasons to be skeptical of the documentary.](https://tribunemag.co.uk/2020/09/the-social-delusion/)

> The claims of Big Tech’s mind-control system are full of tells that the
> enterprise is a con. For example, the reliance on the “Big Five” personality
> traits as a primary means of influencing people even though the “Big Five”
> theory is unsupported by any large-scale, peer-reviewed studies and is mostly
> the realm of marketing hucksters and pop psych.
>
> Big Tech’s promotional materials also claim that their algorithms can
> accurately perform “sentiment analysis” or detect peoples’ moods based on
> their “microexpressions,” but these are marketing claims, not scientific ones.
> These methods are largely untested by independent scientific experts, and
> where they have been tested, they’ve been found sorely wanting.
> Microexpressions are particularly suspect as the companies that specialize in
> training people to detect them have been shown to underperform relative to
> random chance.

Cory Doctorow
[gets to the heart of it](https://onezero.medium.com/how-to-destroy-surveillance-capitalism-8135e6744d59)
in the above when digging into the veracity behind the claims big tech makes
about the power of their software. Who then is served by fearmongering around
the power of that software rather than the power of these vertically and/or
horizontally integrated firms that have wholly captured regulators and public
opinion?

In mainstream discourse, Russiagate served to foster fear, uncertainty and doubt
about the information available on the internet; just as Russian state-backed
actors allegedly used sockpuppets to foster fear, uncertainty and doubt around
the 2016 election. "A Social Dilemma" turned that fear around algorithm
manipulation in particular up to 11. But the tactic has much deeper roots than
the fallout from the so-called US electing its second Hollywood star to serve as
the head of the executive branch.

## Fear, Uncertainty, and Doubt

When contemporary media covers "misinformation campaigns", they're rebranding
the exact same tactic favored by monopolists to hamstring competition in the
Anglosphere for the better part of a century. That tactic is known as Fear,
Uncertainty and Doubt (FUD).

Though there's a lengthier history of the phrase in the english speaking
world[^3], FUD's relevance to this particular topic begins with the way
[Microsoft perfected the FUD-as-anticompetitive-tactic that IBM pioneered](https://web.archive.org/web/20190114060827/http://www.cavcomp.demon.co.uk/halloween/fuddef.html):

> Microsoft soon picked up the art of FUD from IBM, and throughout the 80's used
> FUD as a primary marketing tool, much as IBM had in the previous decade. They
> ended up out FUD-ding IBM themselves during the OS2 vs Win3.1 years.
>
> A good example of MS FUD, and its potential, was demonstrated when Digital
> Research launched their DR DOS against MS-DOS5. DR-DOS offered more features
> and cost less, and was widely acclaimed by all. Then the new MS windows 3.1
> release flashed up a trivial error message when run under DR DOS, and all of a
> sudden everybody was saying DR DOS is great but you can have problems running
> Windows on it. At the same time Microsoft announced the 'imminent' release of
> MS DOS6 which would be far more feature packed than DR DOS. In reality they
> had nothing, they had only just started looking at a 'DOS 6' in response to
> the DR launch, and it is also questionable whether the MS product was better.
> This classic FUD pack occurred together with a dealer package designed to make
> it financially advantageous to offer MS DOS with windows, and the result is
> history. Many believe this was the making of the MS monopoly.

As it turns out, that tactic--the search for a way to generate an apparent
failure regardless of actual functionality, in order to organically create FUD
amongst
customers--[came from Bill himself](https://web.archive.org/web/20180805184350/http://www.maxframe.com/DR/Info/fullstory/factstat.html#intendm):

> You never sent me a response on the question of what things an app would do
> that would make it run with MSDOS and not run with DR-DOS. Is there any
> version check or api that they fail to have? Is ther feature they have that
> might get in our way? I am not looking for something they cant get around. I
> am looking for something that their current binary fails on.
>
> This is a fairly urgent question for me and I have received nothing.

Bill Gates is a sharp guy. To act as ruthelessly as he's acted in the last fifty
years and not produce a paper trail is remarkable, regarless of the reputation
management services available to someone with his wealth. This email (probably
our best evidence from Gates himself for his anticompetitive business strategy)
is no exception. On first read it seems like he's just pushing the devs to dig
for an incompatibility where DR-DOS fails to run Windows and Microsoft can take
advantage of such.

Of course, as noted above and by Microsoft devs at the time[^4], no such
incompatibility existed, but it's important to understand from the email that
Bill _wasn't looking for an incompatibility_, which becomes much more clear once
we resolve the double-negative:

> I am looking for something they can get around. I am looking for something
> that their current binary fails on.

So that's what FUD looked like 30 years ago. Now imagine a world where instead
of generating FUD organically as above or at the very least from paid actors and
the credulous folks that parrot them (no one ever got fired for buying IBM),
firms now offer services to distribute that generation to multiple sockpuppets
managed by software,
[wetware](https://www.merriam-webster.com/dictionary/wetware), or both, and the
only limit on the number of useful idiots their words can reach is internet
connectivity.

## A Case Study: Today's FUD

Unsurprisingly, Gates has continued using one of his primary anti-competitive
tactics to entrench his personal brand. The following tweet was the inspiration
for this entire post.

| [![Nitter screenshot: replies to a well-sourced tweet casting uncertainty and doubt.](/img/personas.png)](https://nitter.net/qalice/status/1353338440708993024#m) |
| :---------------------------------------------------------------------------------------------------------------------------------------------------------------: |
|                                              Spot the bots - https://nitter.net/qalice/status/1353338440708993024#m                                               |

Despite the previous section, this post is _not_ a deep dive into the laundering
of Bill Gates's reputation in the last twenty years.
[Other people have already done that.](https://citationsneeded.libsyn.com/episode-45-the-not-so-benevolent-billionaire-bill-gates-and-western-media)[^5]
This post is about how to interact with the internet without turning into the
two bootlickers in the screenshot above. They could be sock puppets--a cursory
review of their timelines wasn't conclusive--but that's beside the point. Even
more than that, it's the _single most counterproductive way to interact with the
internet_. **There is no difference between the commentariat content produced by
a useful idiot and a sock puppet.** As an individual, sleuthing around someone's
timeline to try to distinguish fake person from real is counterproductive. That
said, it's 9 am in Moscow sweaty did you just start your shift?

With that out of the way, let's look at what we can do when we venture into the
entirely (don't worry, I'll get pretty thorough on how entire it is) artificial
world that is the [Alexa top-500](https://www.alexa.com/topsites).

## Persona Management

This is my strategy for managing my interactions with these rubes and personas
in the Potemkin Village we used to call Web 2.0:

1. Identify any actor likely to employ sock puppets to generate misinformation
   (e.g. to astroturf public sentiment).
2. Before parroting something we see from a twitter replyguy, hypothesize
   exactly what the misinformation those actors want to spread would look like.
   Regularly interrogate those hypotheses.
3. Ruthelessly interrogate our own biases, sources of information, and axioms we
   belive to be true.
4. Mock and ridicule sock puppets and useful idiots in public spaces, as we have
   the time and energy.
5. To preserve mental health, spend as little time (ideally none) in the
   Astroturfed Web as possible.

### The First Step is Admitting We Have a Problem

Sorry Dorothy, if you made it this far you're not in Kansas anymore. Let's rip
the bandaid off:

- The [HBGary and Stratfor](https://wikileaks.org/hbgary-emails/press-release)
  leaks gave us a tiny window into the way firms like those two and others
  (notably, Palantir[^6]) sell these services to private entities like the US
  Chamber of Commerce and Bank of America as they targeted Glenn Greenwald and
  Wikileaks. In a world where
  [Coca-Cola was funding Colombian death squads at least as recently as 2002](https://www.theguardian.com/world/2001/jul/21/julianborger),
  it's a safe bet that any large firm has the resources, motivation and
  disregard for human life (much less ethics) that they employ these services
  either directly or by way of membership in the Chamber of Commerce.
- A seemingly-innocuous (now infamous in public hacktivist forums)
  [Reddit blog post in 2013 about reddit 'meetup day'](https://redditblog.com/2013/05/08/get-ready-for-global-reddit-meetup-day-plus-some-stats-about-top-reddit-cities-and-languages/)
  acknowledged Edgin Air Force Base, FL as its most 'addicted' (e.g. highest
  traffic volume per capita) city, the same base that's
  [_published research on influencer network effects_](http://arxiv.org/pdf/1402.5644.pdf).
  If he wasn't already, [Aaron Swartz](http://www.aaronsw.com/weblog/shifting3)
  started spinning in his grave that day and
  [hasn't stopped since](https://www.chapo.chat/post/56333).
- The cover image on the top of this post is
  [an actual DoD contract for Persona Management software and services](https://www.google.com/url?q=https://archive.is/mVn1c&sa=D&source=hangouts&ust=1611647203261000&usg=AFQjCNFKgbDyjWEISCqCFO-S8iopKFJogA).
  Purportedly 'only' to be used
  [outside the Anglosphere](https://en.wikipedia.org/wiki/Operation_Earnest_Voice)
  (you know, what we're all up in arms about Russia allegedly doing for the last
  5 years), that lie has been thoroughly debunked by the efforts of
  [Lulzsec](https://www.thenation.com/article/archive/how-spy-agency-contractors-have-already-abused-their-power/),
  Barrett Brown, Blueleaks and so many others, all of whom have been subject to
  political persecution and/or violence, including assassination[^7].
- [Cheap assholes like Scott Adams run sock puppets themselves, manually.](https://comicsalliance.com/scott-adams-plannedchaos-sockpuppet/)
  Think of them as bespoke artisanal small-batch astroturf producers.
- Speaking of individuals, in my experience being extremely online since I was a
  preteen, Gates and Musk have the most sus online discourse surrounding them.
  Bezos, Zuckerberg, Dorsey, et al. seem content to be villains with uncritical
  tech journalists and silicon valley hypebeasts serving as sufficient
  propaganda outlets. Broadly, any billionaire has the means and motivation to
  employ these services in the same way as their corporations.

### Material Analysis

From those bullets, it's clear that the wealthy, powerful and insane (hi Scott),
regardless of state recognition or place on the US Terror Watchlist[^8], are
either confirmed or likely to engage in persona management. The identification
of FUD that supports those groups and individuals needs to be rooted in an
analysis of what information threatens their power and the FUD that combats that
information.

If you struggled to #SpotTheBots in the twitter screenshot above, try returning
to it with that lens--OP is information from an HMO news wire (i.e. a reputable
source; though KHN could very well have its own interests in publishing that
article critical of Oxford/Gates) that threatens Bill Gates's power (his
reputation as a benevolent philanthropist interested in global health and
busting teacher's unions[^5]). In that light, ask yourself which comments sow
uncertainty or doubt about that information in entirely unsubstantiated ways or
in ways that don't _actually_ invalidate it.

### Combat Astroturfism

First, we that live in the anglosphere must combat FUD in ourselves. We need to
identify opinions we hold that have no basis other than that we saw them while
doomscrolling the feed one day. When our unsubstantiated memes, retweets, shares
and driveby comments serve oppresors, this is a type of astroturfism. If we're
lucky, someone will point that out to us, which gets us to the second aspect.

Wherever we see misinformation being spread online, we must dunk[^9] on the rube
who would carry water for those who clearly have the means to carry it for
themselves. To scroll past misinformation without signaling it to the rest of
those doomscrolling is the second type of astroturfism.

To make the mistake of engaging this FUD in good faith is another type. But that
is
[beyond the scope](https://www.youtube.com/playlist?list=PLJA_jUddXvY7v0VkYRbANnTnzkA_HMFtQ)
of this already lengthy rant.

### Leave the Walled Gardens to the Birds

That fifth step in my list above is also beyond the scope of this rant and
itself the subject of a post I've been meaning to write for four years now. I'll
update this with a link to that post once I do write it, but in the meantime I
suggest reading about the [Fediverse](https://fediverse.party/) and the
[IndieWeb](https://indieweb.org/).

[^1]:
    An empire with a long history of Russo- and Sinophobic propaganda would
    _never_
    [leverage that ahistorical understanding of astroturfing](https://thegrayzone.com/2021/01/05/uk-judge-cia-spying-assange-ecuador-cnn/)
    to silence critics and re-entrench its hegemony.

[^2]: Nothing up with that RA.
[^3]:
    [As far back as 1693, roughly.](https://en.wikipedia.org/wiki/Fear,_uncertainty,_and_doubt#cite_note-Payne_1695-3)

[^4]:
    [From the same discovery in _Caldera v Microsoft_, a reply to the inline-quoted Gates email:](https://web.archive.org/web/20180805184350/http://www.maxframe.com/DR/Info/fullstory/factstat.html#intendm)

    > Here follow the three "differences" (between DR and MS DOS) that Aaron has
    > been able to find so far. Except for these differences, the two OSs behave
    > similarly, including undocumented calls.
    >
    > The bottom line is that, given Aaron's current findings, an application
    > can identify DR DOS. However, most apps usually have no business making
    > the calls that will let them decide which DOS (MS or DR) they are running
    > on.

[^5]:
    My personal favorite is that the Foundation is in the business of
    ghostwriting TV episodes for noted copaganda classic Law & Order.

[^6]:
    If I'd done better interviewing there you probably wouldn't be reading this,
    but odds are good you'd be reading about me as the subject of
    [a piece like this](https://www.buzzfeednews.com/article/alexkantrowitz/how-saudi-arabia-infiltrated-twitter).

[^7]:
    [Rest in Power Daphne](https://www.theguardian.com/world/2017/oct/16/malta-car-bomb-kills-panama-papers-journalist)

[^8]: s/o East Turkestan Islamic Movement
[^9]:
    o7 @Tracytron_0w0  
    ![Tweet reply: Quit licking boots](/img/dunk.png)
