---
title: "Read the Fucking Code"
hero_image: "hero.jpg"
avatar: "portrait.jpg"
date: 2020-08-10T07:15:04-07:00
categories:
- writing
draft: true
---

https://www.reddit.com/r/sysadmin/comments/i6y3cs/rant_tech_blogs_polluting_google/g0zswt5/?context=3

> Most major technology platform have Best practices guides, official documentation, and other knowledge bases that are written by the people that design the system, that is the source of truth

Unless we're talking about open source software. I've had to cross-check the code for undefined/unexplained behavior from reading the manual one too many times to call documentation from anyone the source of truth.

Folks here love to whine about the Technet/MSDN overhaul to docs., but all the complaints here are valid only in the pre-docs. MS world. MSDN was great for devs & the .NET docs are still better than any other language I've used, but for ops it was...rough. Being able to see the issues other folks have raised on the particular doc I'm looking at has been a game-changer.
