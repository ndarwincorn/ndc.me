---
title: "The Automator's Creed"
hero_image: "duck.jpg"
date: 2020-02-29T08:11:00-08:00
description: This is my duck. There are many like it, but this one is mine.
categories:
- writing
tags:
- short-form
- litany
- process
draft: false
---
My duck is my best friend. It is my life. I must master it as I must master my life.  
Without me, my duck is useless. Without my duck, I am useless. I must
talk to my duck continuously. I must say more words to my duck than my
internal monologue who is trying to distract me. I must talk to my duck
before I talk to myself. I will ...  
My duck and I know that what counts in software is not the lines we
type, nor the noise of our keyboard, nor the pull requests we submit. We
know that it is the solutions that count. We will solve ...  
My duck is human, even as I, because it is my work. Thus, I will learn
it as a sibling. I will learn its weaknesses, its strength, its
reticence, its certitude, its verbosity and its brevity. I will keep my
duck by my right hand, even as I am right-handed. We will become part
of each other. We will ...  
Before the universe, I swear this creed. My duck and myself are the
solvers of our problems. We are the masters of ourselves. We are the
saviors of my work.  
So be it, until there is no problem, but peace. Amen.
