---
title: "{{ replace .Name "-" " " | title }}"
hero_image: "hero.jpg"
avatar: "portrait.jpg"
date: {{ .Date }}
categories:
- writing
draft: true
---

## Header
