---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
categories:
- about
nometadata: true
notags: true
noshare: true
nocomments: true
draft: true
---

## Header
