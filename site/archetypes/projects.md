---
title: "{{ replace .Name "-" " " | title }}"
hero_image: "hero.jpg"
avatar: "portraits.jpg"
date: {{ .Date }}
categories:
- projects
draft: true
---

## Header
